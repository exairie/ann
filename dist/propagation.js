'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.updateWeights = updateWeights;
exports.backwardPropagate = backwardPropagate;
exports.forwardPropagate = forwardPropagate;

var _index = require('./index2');

var _functions = require('./functions');

function updateWeights(network, input) {
    for (var i in network) {
        /** In Hidden Layer */
        if (i != 0) {
            input = network[i].map(function (o) {
                return o.value;
            });
        }

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = network[i][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var neuron = _step.value;

                for (var v in input) {
                    neuron.weights[v] += _index.learningRate * neuron.delta * v;
                }
                //Bias Weight
                neuron.weights[neuron.weights.length - 1] += _index.learningRate * neuron.delta;
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }
    }
}
function backwardPropagate(network, expected) {
    for (var i = network.length - 1; i >= 0; i--) {
        var error = [];
        var clayer = network[i];
        if (i == network.length - 1) {
            /**
             * this is the output layer
             * compare last layer's output with the expected output
             */
            for (var n in clayer) {
                var cneuron = clayer[n];
                // console.log(clayer)
                var err = expected[n] - cneuron.value;
                // console.log(`Error : Should be ${expected[n]} but ${cneuron.value} instead ${err}`)
                error.push(err);
            }
        } else {
            /**
             * this is the hidden layers
             * compare this layer to the layer to the right
             * check it's delta
             */
            for (var _n in clayer) {
                var er = 0.0;
                var nextLayer = network[i + 1];
                /** Get neurons from next layer */
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = nextLayer[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var nn = _step2.value;

                        er += nn.weights[_n] * nn.delta;
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }

                error.push(er);
            }
        }

        for (var _n2 in clayer) {
            var neuron = clayer[_n2];
            neuron.delta = error[_n2] * (0, _functions.actDerivative)(neuron.value);

            // console.log(`Neuron Delta ${error[n]} * ${sigmoidDerivative(neuron.value)} = ${clayer[n].delta}`)
        }
    }
}
function forwardPropagate(network, inputLayer) {
    for (var i in network) {
        var isfirst = i == 0;
        var layer = network[i];
        if (i != 0) {
            var prevLayer = network[i - 1];
            for (var n in layer) {
                var cneuron = layer[n];
                /**
                 * Neuron for current layer is layer[n]
                 * we want the CURRENT LAYER's WEIGHT to be 
                 * calculated with the previous layer's 
                 * output
                 */
                /**
                 * Activation value = n1w1 * n2w2 + bias
                 */
                var activationValue = 0;
                activationValue += _index.bias; /** * cneuron.weights[prevLayer.length] */
                for (var pn in prevLayer) {
                    // console.log(`Activating ${pn} ${cneuron.weights[pn]} * ${prevLayer[pn].value}`)
                    activationValue += cneuron.weights[pn] * prevLayer[pn].value;
                }
                // console.log(`Neuron before activated ${activationValue}`)

                /**
                 * Use sigmoid as activation function
                 */
                cneuron.value = (0, _functions.activation)(activationValue);
                // console.log(`Current Neuron on layer ${i} : ${cneuron.value}`)
                // console.log(`Current Neuron : Layer ${i} Neuron ${n}`)
            }
        } else if (i == 0) {
            var _prevLayer = inputLayer;
            for (var _n3 in layer) {
                var _cneuron = layer[_n3];
                /**
                 * Neuron for current layer is layer[n]
                 * we want the CURRENT LAYER's WEIGHT to be 
                 * calculated with the previous layer's 
                 * output
                 */
                /**
                 * Activation value = n1w1 * n2w2 + bias
                 */
                var _activationValue = 0;
                _activationValue += _index.bias; /** * cneuron.weights[prevLayer.length] */
                for (var _pn in _prevLayer) {
                    // console.log(`Activating ${pn} ${cneuron.weights[pn]} * ${prevLayer[pn]}`)
                    _activationValue += _cneuron.weights[_pn] * _prevLayer[_pn];
                }
                // console.log(`Neuron before activated ${activationValue}`)

                /**
                 * Use sigmoid as activation function
                 */
                _cneuron.value = (0, _functions.activation)(_activationValue);
            }
        }
    }
    return network[network.length - 1].map(function (o) {
        return o.value;
    });
}
//# sourceMappingURL=propagation.js.map