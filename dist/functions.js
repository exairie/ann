'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.activation = activation;
exports.actDerivative = actDerivative;
exports.sigmoid = sigmoid;
exports.sigmoidDerivative = sigmoidDerivative;
exports.tanh = tanh;
exports.tanhD = tanhD;
exports.reLU = reLU;
exports.reLuD = reLuD;
exports.createDefaultLayer = createDefaultLayer;
exports.nulArray = nulArray;
exports.createArray = createArray;
exports.bar = bar;
exports.log = log;
exports.initNetworks = initNetworks;
exports.printNetwork = printNetwork;
exports.getOutput = getOutput;
exports.getMaxArrayIndex = getMaxArrayIndex;

var _layer3 = require('./layer');

var _layer4 = _interopRequireDefault(_layer3);

var _domain = require('domain');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function activation(t) {
    // return sigmoid(t)
    return tanh(t);
    // return reLU(t)
}
function actDerivative(t) {
    // return sigmoidDerivative(t)
    return tanhD(t);
    // return reLuD(t)
}
function sigmoid(t) {
    return 1 / (1 + Math.pow(Math.E, -t));
}
function sigmoidDerivative(output) {
    return output * (1 - output);
}
function tanh(val) {
    return Math.tanh(val);
}
function tanhD(val) {
    return 1 - Math.pow(val, 2);
}
function reLU(val) {
    return Math.max(0, val);
}
function reLuD(val) {
    // return 1 / Math.log(1 + Math.pow(Math.E,val))
    return val < 0 ? 0 : val > 0 ? 1 : 0.000001;
}
function createDefaultLayer(numOfNeurons) {
    var arr = [];
    for (var i = 0; i < numOfNeurons; i++) {
        arr.push(1);
    }

    return [];
}
function nulArray(sz) {
    var arr = [];
    for (var i = 0; i < sz; i++) {
        arr.push(0);
    }
    return arr;
}
function createArray(sz) {
    var random = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var currentNeuron = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

    /**
     * function to generate an N-sized array
     * used to initialize the weights
     */
    var arr = [];
    /** note that the limit is SZ + 1, the +1 is the weight of bias */
    for (var i = 0; i < sz + 1; i++) {
        if (random) {
            // arr.push((Math.random() * (sz - currentNeuron)) * Math.sqrt(2/currentNeuron))
            /** define intial weights */
            arr.push(Math.random() * 0.5 - 0);
        } else {
            arr.push(1);
        }
    }
    return arr;
}
function bar() {
    console.log("--------------------------------------------");
}
function log(str) {
    process.stdout.write(str);
}
function createLayer(neurons, nextLayerNeurons) {
    var randomizeWeight = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    /**
     * initializing neuron's weights
     * first parameter [neurons] is the number of previous layer's 
     * neuron
     * 
     * so to initialize this layer neuron's weights, we need to get prev layers
     * neuron count
     */
    var arr = [];
    for (var i = 0; i < neurons; i++) {
        arr.push({
            value: 0,
            weights: createArray(nextLayerNeurons, randomizeWeight, neurons)
        });
    }

    return arr;
}

function initNetworks(inputLayer, hiddenLayers, outputLayers) {
    var numOfHiddenLayers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 1;

    /**
     * initializing network
     * network will have architecture as follows
     * first Hidden Layer
     *      + neuron
     *         - input layer count as weight
     *      + neuron
     *         - input layer count as weight 
     * N Hidden Layer
     *      + neuron
     *         - prev hidden layer count as weight
     *      + neuron
     *         - prev hidden layer count as weight 
     * output Layer
     *      + neuron
     *         - last hidden layer count as weight
     *      + neuron
     *         - last hidden layer count as weight 
     */
    /** initialize the network array */
    var networks = [];
    /** creating first hidden layer with weights of input layer */
    networks.push(createLayer(hiddenLayers, inputLayer, true));
    /** loop until all hidden layers is completed */
    // for(let i = 0; i < numOfHiddenLayers - 1; i++){
    //     /** Note that layer neurons' weights count = prev layer neuron count */
    //     networks.push(createLayer(hiddenLayers,hiddenLayers,true))
    // }
    // networks.push(createLayer(47,hiddenLayers,true))    
    /** output layer */
    networks.push(createLayer(outputLayers, hiddenLayers, true));

    return networks;
}
function printNetwork(network) {
    var maxNeuron = 0;
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = network[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var _layer2 = _step.value;

            if (_layer2.length > maxNeuron) maxNeuron = _layer2.length;
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }

    var s = [];
    for (var layer in network) {
        s.push('Layer ' + layer);
    }
    console.log(s.join('\t'));
    for (var i = 0; i < maxNeuron; i++) {
        var strs = [];
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = network[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var _layer = _step2.value;

                if (i >= _layer.length) {
                    strs.push(" ");
                } else {
                    strs.push(_layer[i].value.toFixed(10));
                }
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }

        console.log(strs.join("\t\t"));
    }
}

function getOutput(network) {
    return network[network.length - 1].map(function (e) {
        return e.value;
    });
}
function getMaxArrayIndex(arr) {
    var maxVal = 0;
    var selected = -1;
    for (var i in arr) {
        if (arr[i] > maxVal) {
            selected = i;
            maxVal = arr[i];
        }
    }

    return selected;
}
//# sourceMappingURL=functions.js.map