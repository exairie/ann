"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require("./functions");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Layer = function () {
    function Layer(networkData, output) {
        var name = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "default";

        _classCallCheck(this, Layer);

        //Each data is a Neuron object, containing value and weight        
        this.bias = 1;
        this.name = name;
        this.data = networkData.map(function (d) {
            return {
                value: d,
                nvalue: d,
                error: 0
            };
        });
        this.output = output.map(function (o) {
            return {
                init: o,
                output: 0,
                value: o,
                error: 0
            };
        });
        var no = this.data.length;
        console.log("Initialize layer with " + no + " data");
        console.log(this.data);
        (0, _functions.bar)();
    }

    _createClass(Layer, [{
        key: "initWeights",
        value: function initWeights(layer) {
            //Num of layer = num of weight per neuron
            console.log("Init weights");
            console.log(this.data);
            (0, _functions.bar)();
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var data = _step.value;

                    data.weights = [];
                    for (var i in layer) {
                        data.weights.push(1);
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }, {
        key: "propagate",
        value: function propagate(layer, inputWeights) {
            console.log("Propagating ");
            // console.log(this)
            // console.log("Into ")
            // console.log(layer)
            (0, _functions.bar)();
            //Per layer = Neuron
            for (var l in layer.data) {
                //applying every each of this layer's neuron
                //to each weight of layers
                var tempValue = 0;
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = this.data[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var n = _step2.value;

                        // console.log("Calculating")
                        // console.log(n)
                        if (inputWeights) {
                            tempValue += n.value * inputWeights[l];
                        } else {
                            tempValue += n.value * n.weights[l];
                        }
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }

                tempValue += layer.bias;
                // console.log("Value : " + tempValue)

                layer.data[l].value = (0, _functions.sigmoid)(tempValue);
            }
        }
    }, {
        key: "finalize",
        value: function finalize(layer) {
            console.log("Finalizing ");
            // console.log(layer.output)
            // console.log(this)
            // console.log("Into ")
            // console.log(layer)
            //Per layer = Neuron
            for (var l in layer.output) {
                //applying every each of this layer's neuron
                //to each weight of layers
                var tempValue = 0;
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                    for (var _iterator3 = this.data[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                        var n = _step3.value;

                        // console.log("Calculating")
                        // console.log(n)
                        tempValue += n.value * n.weights[l];
                    }
                } catch (err) {
                    _didIteratorError3 = true;
                    _iteratorError3 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                        }
                    } finally {
                        if (_didIteratorError3) {
                            throw _iteratorError3;
                        }
                    }
                }

                tempValue += layer.bias;
                // console.log("Value : " + tempValue)
                layer.output[l].value = (0, _functions.sigmoid)(tempValue);
            }
            (0, _functions.bar)();
            // console.log(layer.output)
            (0, _functions.bar)();
        }
    }]);

    return Layer;
}();

exports.default = Layer;
//# sourceMappingURL=layer.js.map