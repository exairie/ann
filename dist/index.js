'use strict';

var _layer = require('./layer');

var _layer2 = _interopRequireDefault(_layer);

var _network = require('./network');

var _network2 = _interopRequireDefault(_network);

var _functions = require('./functions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var network = new _network2.default();

network.addLayer((0, _functions.createDefaultLayer)(3));
network.addLayer((0, _functions.createDefaultLayer)(15));
network.addLayer((0, _functions.createDefaultLayer)(20));
network.addLayer((0, _functions.createDefaultLayer)(20));
network.addLayer((0, _functions.createDefaultLayer)(4));

network.train(new _layer2.default([3, 0], [0.2], "input 1"));
console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
network.train(new _layer2.default([1, 2], [0.35], "input 2"));
console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
network.train(new _layer2.default([5, 2], [0.7], "input 3"));
console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
network.train(new _layer2.default([1, 2], [0.45], "input 4"));
console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

(0, _functions.bar)();

network.conclude(new _layer2.default([3, 0], [0]));
network.conclude(new _layer2.default([1, 2], [0]));
network.conclude(new _layer2.default([5, 2], [0]));
network.conclude(new _layer2.default([1, 2], [0]));

var i = 0;
console.log("Input Weights");
console.log(network.inputWeights);
network.layers.forEach(function (l) {
    console.log("Layer " + i++);
    l.data.forEach(function (data) {
        console.log(data);
    });
});
console.log("Output Neuron");
console.log(network.output);
// console.log(network.layers)
//# sourceMappingURL=index.js.map