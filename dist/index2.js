'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.minEpoch = exports.maxEpoch = exports.learningRate = exports.bias = undefined;

var _functions = require('./functions');

var _propagation = require('./propagation');

var _nodejsScanf = require('nodejs-scanf');

var _letter = require('./data/letter');

var letterdata = _interopRequireWildcard(_letter);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var bias = exports.bias = 0;
// import data from './data/enb'
var learningRate = exports.learningRate = 0.1;
var maxEpoch = exports.maxEpoch = 10000;
var minEpoch = exports.minEpoch = 2000;
var data = [[2.7810836, 2.550537003, 0], [1.465489372, 2.362125076, 0], [3.396561688, 4.400293529, 0], [1.38807019, 1.850220317, 0], [3.06407232, 3.005305973, 0], [7.627531214, 2.759262235, 1], [5.332441248, 2.088626775, 1], [6.922596716, 1.77106367, 1], [8.675418651, -0.242068655, 1], [7.673756466, 3.508563011, 1]];
function createOutput(letter) {
    // console.log(`Check letter ${letter.toLowerCase()}`)
    switch (letter.toLowerCase()) {
        case 'a':
            return [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'b':
            return [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'c':
            return [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'd':
            return [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'e':
            return [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'f':
            return [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'g':
            return [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'h':
            return [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'i':
            return [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'j':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'k':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'l':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'm':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'n':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'o':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'p':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'q':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        case 'r':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0];
        case 's':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0];
        case 't':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0];
        case 'u':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0];
        case 'v':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0];
        case 'w':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0];
        case 'x':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0];
        case 'y':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0];
        case 'z':
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
        default:
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
}
// let data = d
var inputCount = 2;
var network = (0, _functions.initNetworks)(inputCount, 4, 2);
console.log(data);

var i = 0;
var llrate = 9999999999;
var clrate = llrate - 1000;
while ((llrate > clrate || true) && i <= maxEpoch) {
    // console.log(`Training : Err rate ${llrate} > ${clrate}`)    
    var errsum = 0;
    for (var _i = 0; _i < data.length; _i++) {
        var arr = data[_i].slice(0, 2);
        var output = (0, _propagation.forwardPropagate)(network, arr);
        // let out = createOutput(data[i][0])
        var out = (0, _functions.nulArray)(2);
        // console.log(out)
        var oneIndex = data[_i][2];
        out[oneIndex] = 1;
        for (var o in out) {
            errsum += Math.pow(out[o] - output[o], 2);
        }
        (0, _propagation.backwardPropagate)(network, out);
        (0, _propagation.updateWeights)(network, arr);
        // console.log(`inpt ${JSON.stringify(arr)} : ${JSON.stringify(output)} exp ${JSON.stringify(out)}`)
    }
    console.log('Epoch ' + (i + 1) + ' lrate : ' + learningRate + ' sum_error : ' + errsum);
    llrate = clrate;
    clrate = errsum;
    if (i % 200 == 0) {
        console.log(i);
    }
    // console.log(`Done : Err rate ${llrate} > ${clrate}`)    
    i++;
}

var test = 10;
console.log('------------------------------');
var sig = (0, _functions.sigmoid)(5);
console.log("Val : Sig  : " + sig);

var exit = false;

console.log("-----------TESTING DATA-------------");
var trueCount = 0;
var allCount = 0;
for (var _i2 = 0; _i2 < data.length; _i2++) {
    var t = false;
    var input = data[_i2].slice(0, 2);
    var outputData = (0, _propagation.forwardPropagate)(network, input);
    var outputValue = (0, _functions.getMaxArrayIndex)(outputData);
    if (data[_i2][2] == outputValue) {
        trueCount++;
    }

    console.log('Iteration ' + (_i2 + 1) + ' Expecting ' + data[_i2][2] + ' result : ' + outputValue + ' ' + JSON.stringify(outputData));
    allCount++;
}
console.log("Accuracy : " + trueCount / allCount);
(0, _functions.printNetwork)(network);

// letterdata.getData(d => {

// })
//# sourceMappingURL=index2.js.map