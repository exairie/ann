'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _layer = require('./layer');

var _layer2 = _interopRequireDefault(_layer);

var _functions = require('./functions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Network = function () {
    function Network() {
        _classCallCheck(this, Network);

        this.layers = [];

        this.sampled = false;
        this.inputWeights = [];
        this.output = [];
    }

    _createClass(Network, [{
        key: 'tderivatife',
        value: function tderivatife(output) {
            return output * (1 - output);
        }
    }, {
        key: 'train',
        value: function train(layer) {
            layer.initWeights(this.layers[0]);
            // console.log("training network using data ")
            // console.log(layer)
            if (this.sampled === false) {
                this.useSample(layer);
                this.sampled = true;
            }
            for (var i = 0; i < 100; i++) {
                this.forwardPropagate(layer);
            }
            // this.layers.unshift(layer)
            // this.layers.push(layer.output)
        }
    }, {
        key: 'conclude',
        value: function conclude(input) {
            console.log("Concluding");
            input.initWeights(this.layers[0]);
            this.forwardPropagate(input, true);
            (0, _functions.bar)();
            console.log("---------------OUTPUT---------------");
            console.log(this.output);
        }
    }, {
        key: 'useSample',
        value: function useSample(layer) {
            for (var i in this.layers[0].data) {
                this.inputWeights.push(1);
            }
            this.output = layer.output;
            layer.initWeights(this.layers[0]);
            console.log("Initializing weight");
            // console.log(layer)
            for (var _i in this.layers) {
                if (_i < this.layers.length - 1) {
                    this.layers[_i].initWeights(this.layers[_i + 1]);
                    // console.log("Initializing weight")
                }
            }
            this.layers[this.layers.length - 1].initWeights(layer.output);

            console.log("done");
            // for(let l of this.layers) console.log(l)
            (0, _functions.bar)();
        }
    }, {
        key: 'addLayer',
        value: function addLayer(layer) {
            this.layers.push(layer);
            // console.log("Adding layer")
        }
    }, {
        key: 'isLastLayer',
        value: function isLastLayer(layer) {
            return this.layers.indexOf(layer) >= this.layers.size - 1;
        }
    }, {
        key: 'isFirstLayer',
        value: function isFirstLayer(layer) {
            return this.layers.indexOf(layer) == 0;
        }
        //Propagating forward as ONE epoch

    }, {
        key: 'forwardPropagate',
        value: function forwardPropagate(inputLayer) {
            var skipbackprop = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            console.log("propagating forward!");
            var layers = this.layers;

            console.log("Input Layer!");
            inputLayer.propagate(layers[0], this.inputWeights);

            for (var i in layers) {
                // console.log("Layer!" + i)
                if (i < layers.length - 1) {
                    layers[i].propagate(layers[i + 1]);
                    // console.log(layers[i])   
                    // console.log(layers[i+1])
                }
                console.log("DONE");
            }

            layers[layers.length - 1].finalize(this);

            if (!skipbackprop) this.backPropagate(inputLayer);
        }
        //Propagating backward as ONE EPOCH

    }, {
        key: 'backPropagate',
        value: function backPropagate(inputLayer) {
            //output layer
            for (var i in this.output) {
                this.output[i].error = /*expected*/inputLayer.output[i].value - this.output[i].data;
                this.output[i].delta = this.output[i].error * this.tderivatife(this.output[i].value);
            }
            for (var _i2 = this.layers.length - 1; _i2 >= 0; _i2++) {
                var layer = this.layers[_i2];
                var nextLayer = this.layers[_i2 - 1];

                if (_i2 === this.layers.length - 1) {
                    var layererrors = this.output;
                    //This is the last layer before output
                    for (var j in layererrors) {
                        layer.data[j].weights[j];
                    }
                }
            }
        }

        /**
         * v1 * w11 = v2
         * 
         */

    }]);

    return Network;
}();

exports.default = Network;
//# sourceMappingURL=network.js.map