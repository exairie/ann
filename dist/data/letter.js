'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getData = getData;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getData(callback) {
    console.log("READDIIIINGGG DATA");
    _fs2.default.readFile('./data/letter-recognition.data', 'utf8', function (err, buffer) {
        var arr = [];
        buffer.split("\n").forEach(function (element) {
            var carr = [];
            element.split(',').forEach(function (e) {
                var val_normalized = parseFloat(e);
                carr.push(!isNaN(e) ? val_normalized : e);
            });
            arr.push(carr);
        });
        callback(arr);
    });
}
//# sourceMappingURL=letter.js.map