import sys

from django.http import HttpRequest, HttpResponse
from django.http import JsonResponse
import json
from . import runner

def index(request):
    return HttpResponse("Hello World")

def tryJson(request):
    returnData = {}
    returnData['helo'] = "hey"    
    print(json.dumps(returnData))
    return JsonResponse(returnData)

def calculateInNetwork(request):

    rwdata = json.loads(request.POST.get('data'))
    data = replaceNumeric(rwdata)
    output = runner.test(1,'tanh',data)

    returnData = {}
    for i in range(len(runner._peminatan)):
        returnData[runner._peminatan[i]] = output[i]
    
    return HttpResponse(json.dumps(returnData))

def replaceNumeric(inputData):
    ndata = []
    for d in inputData :
        ndata.append(bobot(d))
    
    return ndata
    

def bobot(x):
    return{
        'A':4,
        'A-':3.7,
        'B+':3.4,
        'B':3,
        'B-':2.7,
        'C+':2.4,
        'C':2,
        'C-':1.7
    }[x]