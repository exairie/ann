_peminatan = ['RPL','TKJ','MM']



def getMaxArrayIndex(arr):
    maxVal = 0
    selected = -1
    for i in range(len(arr)):
        if(arr[i] > maxVal):
            selected = i
            maxVal = arr[i]

    return selected
def peminatanParseOutput(arr):
    return _peminatan[getMaxArrayIndex(arr)]

def loadNetwork():
    network = None
    with open('networks/%s' % activation, 'r') as net:
        network = json.loads(net.read())

    return network

def test(bias, activation, data):
    # initialize counter
    network = loadNetwork(activation)
    rightCount = 0  # for right prediction
    allCount = 0  # for all data
    # testResultPath = 'test/%d/%.6f/%d' % (epochNumber, constants.getLearningRate(), constants.getBias())
    # makedirs(testResultPath, exist_ok=True)
    # logTest = open('%s/%s_testresult.txt' % (testResultPath, activation), mode='a')
    

    arr = data
    print("Data %4d = " % (i), end="", flush=True)
    # begin forward propagation
    output = forward(network, arr)
    # now we parse the result using custom function
    result = getMaxArrayIndex(output)
    exp = data
    out = peminatanParseOutput(output)

    # get the result
    print("expected %4s result %4s | Reg : %s" %
            (exp, out, "%.10f, %.10f, %.10f" % tuple(output)))                                

def forward(network, input):
    for i in range(len(network)):
        useSoftmax = False

        isFirst = i == 0
        layer = network[i]
        # print("layer : %s %d" % (list(map(lambda x : x['value'],layer)), i))
        inpt = []
        if not isFirst:
            prevLayer = network[i - 1]
            inpt = list(map(lambda x: x['value'], prevLayer))
        elif isFirst:
            prevLayer = input
            inpt = prevLayer
        for n in range(len(layer)):
            cneuron = layer[n]
            # Neuron for current layer is layer[n]
            # we want the CURRENT LAYER's WEIGHT to be
            # calculated with the previous layer's
            # output

            # Activation value = n1w1 * n2w2 + bias
            activationValue = 0
            activationValue += getBias() * \
                cneuron['weights'][len(cneuron['weights']) - 1]

            # pv = addVector(np.asfarray(cneuron['weights']), )
            for pn in range(len(prevLayer)):
                # print(activationValue)
                activationValue += cneuron['weights'][pn] * \
                    inpt[pn]
            
            if cneuron['activation'] == 'sigmoid':
                cneuron['value'] = functions.sigmoid(activationValue)
            elif cneuron['activation'] == 'tanh':
                cneuron['value'] = functions.tanh(activationValue)
            elif cneuron['activation'] == 'relu':
                cneuron['value'] = functions.relu(activationValue)
            elif cneuron['activation'] == 'softmax':
                useSoftmax = True
                cneuron['activationValue'] = activationValue
            else:
                # default is sigmoid
                cneuron['value'] = functions.sigmoid(activationValue)
        if useSoftmax == True:
            accumulated = 0

            x = np.asfarray(list(map(lambda x: x['activationValue'], layer)))
            xo = np.exp(x - np.max(x))
            xout = xo / xo.sum()

            for n in range(len(layer)):
                layer[n]['value'] = xout[n]            

    return list(map(lambda x: x['value'], network[-1]))