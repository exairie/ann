from random import random
from math import e
import math
import math
import numpy as np

_peminatan = ['RPL','TKJ','MM']

def CrossEntropy(yHat, y):
    if(yHat <= 0):
        return 0
        pass
    if(y <= 0):
        pass    
    return - math.log(yHat) * y

def softmax(x):
    return np.exp(x) / np.sum(np.exp(x), axis=0)

def softmaxD(val):
    return val * (1 - val)

def randomWeights():
    return random() - 0.5
    
def createLayer(numOfNeuron, lastLayerCount, activation):
    return [{"activation":activation,"value":0, "weights":[randomWeights() for i in range(lastLayerCount + 1)] } for i in range(numOfNeuron) ]

def relu(val):
    # return math.log(1.0 + e ** val)
    return max(0.01 * val,val)

def reluD(val):
    return 1 if val > 0 else 0.01 if val < 0 else 0.0000001

    # return 1.0/(1.0 + e ** -val)
def sigmoid(val):
    return 1/(1 + e**-val)

def sigmoidD(output):
    return output * (1-output)

def tanh(val):
    return math.tanh(val)

def tanhD(val):
    return 1-(val**2)

def letterDataCreateOutput(letter):                
         return {
            'a' : [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'b' : [0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'c' : [0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'd' : [0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'e' : [0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'f' : [0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'g' : [0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'h' : [0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'i' : [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'j' : [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'k' : [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'l' : [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'm' : [0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'n' : [0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
            'o' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
            'p' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
            'q' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
            'r' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
            's' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
            't' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
            'u' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0],
            'v' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
            'w' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0],
            'x' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0],
            'y' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0],
            'z' : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]
        }.get(letter,[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])          

def letterData():
    with open('data/letter-recognition.data') as d:
        data = d.readlines()
        rdata = []
        for lc in data:
            line = lc.replace('\n','')
            ldata = []
            for c in line.split(','):                
                if str.isdigit(c): ldata.append(int(c))
                else: ldata.append(c)
            rdata.append(ldata)
        
        return rdata
def timeIntervalStr(time):
    hour = int(time / 3600)
    time -= hour * 3600

    minute = int(time / 60)
    time -= minute * 60

    return "%2d hour %2d min %2d sec" % (hour, minute, time)
def peminatanTest():
    with open('data/generated_test.data') as d:
        data = d.readlines()
        rdata = []
        for lc in data:
            line = lc.replace('\n','')
            ldata = []
            for c in line.split(','):                
                try:
                    el = float(c)
                except ValueError:
                    el = c
                
                ldata.append(el)
            
            rdata.append(ldata)
        
        return rdata
def peminatan():
    with open('data/generated.data') as d:
        data = d.readlines()
        rdata = []
        for lc in data:
            line = lc.replace('\n','')
            ldata = []
            for c in line.split(','):                
                try:
                    el = float(c)
                except ValueError:
                    el = c
                
                ldata.append(el)
            
            rdata.append(ldata)
        
        return rdata
def peminatanCreateOutput(pem):
    return {
        'rpl' : [1,0,0],
        'tkj' : [0,1,0],
        'mm' : [0,0,1]
    }.get(pem.lower(), [0,0,0])

def getMaxArrayIndex(arr):
    maxVal = 0
    selected = -1
    for i in range(len(arr)):
        if(arr[i] > maxVal):
            selected = i
            maxVal = arr[i]

    return selected

def peminatanParseOutput(arr):
    return _peminatan[getMaxArrayIndex(arr)]
