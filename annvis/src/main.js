import Vue from "vue";
import App from "./App.vue";
import "./styles/app.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import BootstrapVue from "bootstrap-vue";
import VueRouter from "vue-router";
import HelloWorld from "./components/HelloWorld.vue";
import Calculate from "./components/Calculate.vue";
Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.config.productionTip = false;
const router = new VueRouter({
  routes: [
    {
      path: "/",
      component: HelloWorld
    },
    {
      path: "/calculate",
      component: Calculate
    }
  ]
});
export const app = new Vue({
  router,
  render: h => h(App)
});
app.$mount("#app");
