'use strict'
import {initNetworks,sigmoid, printNetwork, nulArray, getMaxArrayIndex} from './functions'
import { forwardPropagate, backwardPropagate, updateWeights } from './propagation'
import {scanf} from 'nodejs-scanf'
// import data from './data/enb'
// import * as letterdata from './data/letter'


export const bias = 0
export const learningRate = 0.1
export let maxEpoch = 10000
export const minEpoch = 2000    
const data = [
    [2.7810836, 2.550537003,0],
	[1.465489372, 2.362125076,0],
	[3.396561688, 4.400293529,0],
	[1.38807019, 1.850220317,0],
	[3.06407232, 3.005305973,0],
	[7.627531214, 2.759262235,1],
	[5.332441248, 2.088626775,1],
	[6.922596716, 1.77106367,1],
	[8.675418651, -0.242068655,1],
	[7.673756466, 3.508563011,1]
]
// let data = d
const inputCount = 2
const network = initNetworks(inputCount,4,2)
console.log(data)

let i = 0
let llrate = 9999999999
let clrate = llrate - 1000
while((llrate > clrate || true) && i <= maxEpoch){
    // console.log(`Training : Err rate ${llrate} > ${clrate}`)    
    let errsum = 0
    for(let i = 0; i < data.length;i++){        
        let arr = data[i].slice(0,2)
        let output = forwardPropagate(network,arr)
        // let out = createOutput(data[i][0])
        let out = nulArray(2)
        // console.log(out)
        let oneIndex = data[i][2]
        out[oneIndex] = 1
        for(let o in out){
            errsum += Math.pow(out[o] - output[o],2)
        }
        backwardPropagate(network, out)
        updateWeights(network,arr)
        // console.log(`inpt ${JSON.stringify(arr)} : ${JSON.stringify(output)} exp ${JSON.stringify(out)}`)
    }
    console.log(`Epoch ${(i+1)} lrate : ${learningRate} sum_error : ${errsum}`)
    llrate = clrate
    clrate = errsum
    if(i % 200 == 0){
        console.log(i)
    }
    // console.log(`Done : Err rate ${llrate} > ${clrate}`)    
    i++
}


let exit = false

let trueCount = 0
let allCount = 0
for(let i = 0; i < data.length;i++){
    let t = false
    let input = data[i].slice(0,2)
    let outputData = forwardPropagate(network, input)
    let outputValue = getMaxArrayIndex(outputData)
    if(data[i][2] == outputValue){
        trueCount++        
    }
    
    console.log(`Iteration ${(i+1)} Expecting ${data[i][2]} result : ${outputValue} ${JSON.stringify(outputData)}`)
    allCount ++
}
console.log("Accuracy : " + (trueCount/allCount))
printNetwork(network)


// letterdata.getData(d => {
    
// })