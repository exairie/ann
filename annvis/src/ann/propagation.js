
import {bias} from '../App'
import {activation, actDerivative} from './functions'
import {learningRate} from '../App'
export function updateWeights(network, input){
    for(let i in network){
        /** In Hidden Layer */
        if(i != 0){
            input = network[i].map(o => o.value)
        }

        for(let neuron of network[i]){
            for(let v in input){
                neuron.weights[v] += learningRate * neuron.delta * v
            }
            //Bias Weight
            neuron.weights[neuron.weights.length - 1] += learningRate * neuron.delta                        
        }
    }
}
export function backwardPropagate(network, expected){
    for(let i = network.length - 1;i >= 0;i--){
        let error = []
        let clayer = network[i]
        if(i == network.length - 1){
            /**
             * this is the output layer
             * compare last layer's output with the expected output
             */
            for(let n in clayer){
                let cneuron = clayer[n]
                // console.log(clayer)
                let err = expected[n] - cneuron.value
                // console.log(`Error : Should be ${expected[n]} but ${cneuron.value} instead ${err}`)
                error.push(err)
            }
        }else{
            /**
             * this is the hidden layers
             * compare this layer to the layer to the right
             * check it's delta
             */
            for(let n in clayer){
                let er = 0.0
                let nextLayer = network[i + 1]
                /** Get neurons from next layer */
                for(let nn of nextLayer){
                    er += nn.weights[n] * nn.delta
                }

                error.push(er)
            }

        }

        for(let n in clayer){
            let neuron = clayer[n]
            neuron.delta = error[n] * actDerivative(neuron.value)

            // console.log(`Neuron Delta ${error[n]} * ${sigmoidDerivative(neuron.value)} = ${clayer[n].delta}`)
        }
    }
}
export function forwardPropagate(network, inputLayer){
    for(let i in network){
        let isfirst = i == 0        
        let layer = network[i]
        if(i !=  0){
            let prevLayer = network[i - 1]
            for(let n in layer){
                let cneuron = layer[n]
                /**
                 * Neuron for current layer is layer[n]
                 * we want the CURRENT LAYER's WEIGHT to be 
                 * calculated with the previous layer's 
                 * output
                 */
                /**
                 * Activation value = n1w1 * n2w2 + bias
                 */
                let activationValue = 0
                activationValue += bias /** * cneuron.weights[prevLayer.length] */
                for(let pn in prevLayer){
                    // console.log(`Activating ${pn} ${cneuron.weights[pn]} * ${prevLayer[pn].value}`)
                    activationValue += cneuron.weights[pn] * prevLayer[pn].value
                }
                // console.log(`Neuron before activated ${activationValue}`)

                /**
                 * Use sigmoid as activation function
                 */
                cneuron.value = activation(activationValue)
                // console.log(`Current Neuron on layer ${i} : ${cneuron.value}`)
                // console.log(`Current Neuron : Layer ${i} Neuron ${n}`)
            }
        }
        else if(i == 0){
            let prevLayer = inputLayer
            for(let n in layer){
                let cneuron = layer[n]
                /**
                 * Neuron for current layer is layer[n]
                 * we want the CURRENT LAYER's WEIGHT to be 
                 * calculated with the previous layer's 
                 * output
                 */
                /**
                 * Activation value = n1w1 * n2w2 + bias
                 */
                let activationValue = 0
                activationValue += bias /** * cneuron.weights[prevLayer.length] */
                for(let pn in prevLayer){
                    // console.log(`Activating ${pn} ${cneuron.weights[pn]} * ${prevLayer[pn]}`)
                    activationValue += cneuron.weights[pn] * prevLayer[pn]
                }
                // console.log(`Neuron before activated ${activationValue}`)

                /**
                 * Use sigmoid as activation function
                 */
                cneuron.value = activation(activationValue)                
            }
        }
    }
    return network[network.length - 1].map(o => o.value)
}
