
// import Layer from './layer';
// import { create } from 'domain';

export function activation(t){
    // return sigmoid(t)
    return tanh(t)
    // return reLU(t)
}
export function actDerivative(t){
    // return sigmoidDerivative(t)
    return tanhD(t)
    // return reLuD(t)
}
export function sigmoid(t) {
    return 1/(1+Math.pow(Math.E, -t));
}
export function sigmoidDerivative(output){
    return output * (1 - output)
}
export function tanh(val){
    return Math.tanh(val)
}
export function tanhD(val){
    return 1 - (Math.pow(val,2))
}
export function reLU(val){
    return Math.max(0,val)
}
export function reLuD(val){
    // return 1 / Math.log(1 + Math.pow(Math.E,val))
    return val < 0?0:val>0?1:0.000001
}
export function createDefaultLayer(numOfNeurons){
    let arr = []
    for(let i = 0; i < numOfNeurons;i++){
        arr.push(1)
    }

    return []    
}
export function nulArray(sz){
    let arr = []
    for(let i = 0; i < sz; i++){
        arr.push(0)
    }
    return arr
}
export function createArray(sz, random = false, currentNeuron = 1){
    /**
     * function to generate an N-sized array
     * used to initialize the weights
     */
    let arr = []
    /** note that the limit is SZ + 1, the +1 is the weight of bias */
    for(let i = 0;i < sz + 1;i++) {
        if(random){
            // arr.push((Math.random() * (sz - currentNeuron)) * Math.sqrt(2/currentNeuron))
            /** define intial weights */
            arr.push((Math.random() * 0.5) - 0)
        }
        else{
            arr.push(1)
        }
    }
    return arr
}
export function bar(){
    console.log("--------------------------------------------")
}
export function log(str){
    process.stdout.write(str)
}
function createLayer(neurons, nextLayerNeurons,randomizeWeight = false){
    /**
     * initializing neuron's weights
     * first parameter [neurons] is the number of previous layer's 
     * neuron
     * 
     * so to initialize this layer neuron's weights, we need to get prev layers
     * neuron count
     */
    let arr = []
    for (let i = 0; i < neurons;i++){
        arr.push({
            value:0,
            weights:createArray(nextLayerNeurons,randomizeWeight,neurons)
        })
    }

    return arr;
}

export function initNetworks(inputLayer, hiddenLayers, outputLayers, numOfHiddenLayers = 1){
    /**
     * initializing network
     * network will have architecture as follows
     * first Hidden Layer
     *      + neuron
     *         - input layer count as weight
     *      + neuron
     *         - input layer count as weight 
     * N Hidden Layer
     *      + neuron
     *         - prev hidden layer count as weight
     *      + neuron
     *         - prev hidden layer count as weight 
     * output Layer
     *      + neuron
     *         - last hidden layer count as weight
     *      + neuron
     *         - last hidden layer count as weight 
     */
    /** initialize the network array */
    let networks = []
    /** creating first hidden layer with weights of input layer */
    networks.push(createLayer(hiddenLayers,inputLayer,true))
    /** loop until all hidden layers is completed */
    // for(let i = 0; i < numOfHiddenLayers - 1; i++){
    //     /** Note that layer neurons' weights count = prev layer neuron count */
    //     networks.push(createLayer(hiddenLayers,hiddenLayers,true))
    // }
    // networks.push(createLayer(47,hiddenLayers,true))    
    /** output layer */
    networks.push(createLayer(outputLayers,hiddenLayers,true))    

    return networks
}
export function printNetwork(network){
    let maxNeuron = 0
    for(let layer of network){        
        if(layer.length > maxNeuron)
            maxNeuron = layer.length
    }
    let s = []
    for(let layer in network){
        s.push(`Layer ${layer}`)
    }
    console.log(s.join('\t'))
    for(let i = 0; i < maxNeuron;i++){
        let strs = []
        for(let layer of network){            
            if(i >= layer.length){
                strs.push(" ")
            }else{
                strs.push(layer[i].value.toFixed(10))
            }                                 
        }
        console.log(strs.join("\t\t"))
    }
}

export function getOutput(network){
    return network[network.length - 1].map(e => e.value)
}
export function getMaxArrayIndex(arr){
    let maxVal = 0
    let selected = -1
    for(let i in arr){
        if(arr[i] > maxVal){
            selected = i
            maxVal = arr[i]
        }
    }

    return selected
}