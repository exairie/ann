from functions import createLayer
import time
from sys import argv
import numpy
from functions import peminatan, peminatanCreateOutput, peminatanParseOutput, getMaxArrayIndex, CrossEntropy, peminatanTest, timeIntervalStr
import json
from os import path, makedirs
import constants
import propagation
from math import pow, floor
import math

logger = None



epochNumber = 40000
currentepoch = 0
activation = "relu"

# Parse the data from file, into an array
data = peminatan()
testData = peminatanTest()

network = None

# Use an ID to save network to disk for each epoch
networkID = 'peminatan-%s' % activation
# Check whether the network is already exist
if path.isfile(networkID):
    # if it is, then we continue with that network
    with open(networkID, 'r') as net:
        network = json.loads(net.read())

else:
    # if not, create a new one
    network = []
    # specify number of neurons, number of previous neurons, and activation
    network.append(createLayer(28, 34, activation))
    network.append(createLayer(32, 28, activation))
    network.append(createLayer(
        3, 15, "softmax" if activation == 'relu' else activation))

# Function to save network to disk


def saveNetwork():
    with open(networkID, 'w') as file:
        file.write(json.dumps(network))


def finishNetwork(start, end):
    with open(networkID, 'a') as file:
        logger.write("Start : %.0f, end : %.0f, Total Time = %.0f Sec" %
                     (start, end, end-start))
        logger.close()


# function to learn
def learn():
    start = time.time()

    # initialize the base limit
    # this limit specify whether the network should continue to learn or not
    llerror = 999999999999
    lerror = 99999999999

    ep = currentepoch

    # log time
    tlast = time.time()

    # so whenever the prev error is lower than the new one, stop the learning process
    # this is to capture local error minimum of the network
    while ep < epochNumber:
        now = time.time()
        print("%s, Epoch %d lrate : %.4f" %
              (activation, ep, constants.getLearningRate()), end="", flush=True)
        # initialize error sum
        errsum = 0.0
        # for i in range(int(len(data) * 0.8)):
        for i in range(len(data)):
            # progress bar, the number of data is quite small so we dont need it
            # if i % floor(len(data) / 25) == 0:
                # print("#",end="", flush=True)

            # slice the data. we need only the data from index 0 to 33
            arr = data[i][0:34]
            # begin forward propagation
            # the return value is the last layer's value, or the output neuron
            output = propagation.forward(network, arr)

            # then we create the desired output
            out = peminatanCreateOutput(data[i][34].lower())

            # summarize the errors
            for o in range(len(output)):
                # errsum += CrossEntropy(output[o], out[o])
                errsum += pow(out[o] - output[o], 2) / 2

            # now propaga backward using the actual output
            # for each of every iteration, error delta is saved into the neuron
            # so we calculate each error individually rather than the complete network
            # this is called stochastic gradient descend
            propagation.backward(network, out)

            # we have the error, so lets update network's weights
            propagation.updateWeights(network, arr)

        # save the network in case of anxiety
        # so we dont have to train the network from the beginning
        # errsum *= -1
        saveNetwork()

        # calculate remaining time
        remaining = (now - tlast) * (epochNumber - ep)

        # print the sum of error
        print(" sum_error : %.16f. Remtime : %s" %
              (errsum, timeIntervalStr(remaining)))
        tlast = now
        logger.write("%.16f\n" % errsum)

        # indicate in what epoch are we in
        ep += 1

        # update the loop parameters
        llerror = lerror
        lerror = errsum

    end = time.time()
    finishNetwork(start, end)


# function to test the data
def test():
    # initialize counter
    rightCount = 0  # for right prediction
    allCount = 0  # for all data
    testResultPath = 'test/%d/%.6f/%d' % (epochNumber, constants.getLearningRate(), constants.getBias())
    makedirs(testResultPath, exist_ok=True)
    logTest = open('%s/%s_testresult.txt' % (testResultPath, activation), mode='a')
    

    for i in range(len(testData)):
        # for i in range(len(data)):
        # slice the data
        arr = testData[i][0:34]
        print("Data %4d = " % (i), end="", flush=True)
        # begin forward propagation
        output = propagation.forward(network, arr)
        # now we parse the result using custom function
        result = getMaxArrayIndex(output)
        exp = testData[i][34]
        out = peminatanParseOutput(output)

        # get the result
        print("expected %4s result %4s | Reg : %s" %
              (exp, out, "%.10f, %.10f, %.10f" % tuple(output)))
              
        logTest.write("expected %4s result %4s | Reg : %s\n" %
              (exp, out, "%.10f, %.10f, %.10f" % tuple(output)))

        if exp == out:
            # increase right counter if the output is the same as the input
            rightCount += 1
        # count all output whether it's right or wrong
        allCount += 1
    # taraaaa
    print("All : %d Right : %d, accuracy : %.4f" %
          (allCount, rightCount, rightCount/allCount))

    logTest.write("All : %d Right : %d, accuracy : %.4f\n" %
          (allCount, rightCount, rightCount/allCount))


# look for execution argument
if len(argv) == 1:
    # open log to continue learning
    try:
        logger = open('log_output_errsum_%s_.txt' % activation, mode='r')
        currentepoch = len(logger.readlines())
        print("Resuming from epoch %d. Enter to continue" % currentepoch)
        te = input()

        logger.close()
    except FileNotFoundError:
        pass

    logger = open('log_output_errsum_%s_.txt' % activation, mode='a')
    # if there isn't any, do learn then test
    learn()
    test()
elif argv[1] == "auto":
    epochNumber = int(argv[2])
    constants.setLearningRate(float(argv[3]))
    constants.setBias(int(argv[4]))

    testOnly = 'no'
    if(len(argv) > 6):
        testOnly = argv[6]


    # if the first argument is not test, then it's the activation function
    activation = argv[5]
    print('''
    Setup:
    Epoch : %d
    Learning Rate : %.6f
    Bias : %d
    Activation : %s
    ''' % (epochNumber, constants.getLearningRate(), constants.getBias(), activation))
    # Reload Network
    # Use an ID to save network to disk for each epoch
    networkPath = "saved/%d/%.6f/%d" % (epochNumber, constants.getLearningRate(), constants.getBias())
    makedirs(networkPath,exist_ok=True)
    networkID = '%s/peminatan-%s' % (networkPath, activation)
    # Check whether the network is already exist
    if path.isfile(networkID):
        # if it is, then we continue with that network
        with open(networkID, 'r') as net:
            network = json.loads(net.read())

    else:
        # if not, create a new one
        network = []
        # specify number of neurons, number of previous neurons, and activation    
        network.append(createLayer(28, 34, activation))
        network.append(createLayer(32, 28, activation))
        network.append(createLayer(3, 32, activation))

    # open log
    try:
        logger.close()
    except AttributeError:
        pass
    loggerFileName = 'saved/%d/%.6f/%d' % (epochNumber, constants.getLearningRate(), constants.getBias())
    makedirs(loggerFileName, exist_ok=True)
    logger = open('%s/log_output_errsum_%s_.txt' % (loggerFileName,activation), mode='a')

    # but the second argument is test, so do test without training
    if testOnly != 'test':
        learn()

    with open(networkID, 'r') as net:
        network = json.loads(net.read())

    test()
    # te = input()
elif argv[1] != 'test' and len(argv) == 2:
    # if the first argument is not test, then it's the activation function
    activation = argv[1]
    # Reload Network
    # Use an ID to save network to disk for each epoch
    networkID = 'peminatan-%s' % activation
    # Check whether the network is already exist
    if path.isfile(networkID):
        # if it is, then we continue with that network
        with open(networkID, 'r') as net:
            network = json.loads(net.read())

    else:
        # if not, create a new one
        network = []
        # specify number of neurons, number of previous neurons, and activation
        network.append(createLayer(9, 33, activation))
        network.append(createLayer(15, 9, activation))
        network.append(createLayer(3, 15, activation))

    # open log

    try:
        logger = open('log_output_errsum_%s_.txt' % activation, mode='r')
        currentepoch = len(logger.readlines())
        print("Resuming from epoch %d. Enter to continue" % currentepoch)
        te = input()

        logger.close()
    except FileNotFoundError:
        pass

    logger = open('log_output_errsum_%s_.txt' % activation, mode='a')

    learn()
    test()
elif argv[1] != 'test' and argv[2] == 'test':
    # if the first argument is not test, then it's the activation function
    activation = argv[1]
    # Reload Network
    # Use an ID to save network to disk for each epoch
    networkID = 'peminatan-%s' % activation
    # Check whether the network is already exist
    if path.isfile(networkID):
        # if it is, then we continue with that network
        with open(networkID, 'r') as net:
            network = json.loads(net.read())

    else:
        # if not, create a new one
        network = []
        # specify number of neurons, number of previous neurons, and activation
        network.append(createLayer(9, 33, activation))
        network.append(createLayer(15, 9, activation))
        network.append(createLayer(3, 15, activation))

    # open log
    try:
        logger.close()
    except AttributeError:
        pass
    logger = open('log_output_errsum_%s_.txt' % activation, mode='a')

    # but the second argument is test, so do test without training
    test()

elif argv[1] == 'test':
    # if the first argument is test, just test the network without training
    test()

else:
    # no option
    learn()
    test()
