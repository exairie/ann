from constants import getBias
# import numpy as np
from constants import getLearningRate
import json
import functions
from math import e
import numpy as np
import math
import functions


def forward(network, input):
    for i in range(len(network)):
        useSoftmax = False

        isFirst = i == 0
        layer = network[i]
        # print("layer : %s %d" % (list(map(lambda x : x['value'],layer)), i))
        inpt = []
        if not isFirst:
            prevLayer = network[i - 1]
            inpt = list(map(lambda x: x['value'], prevLayer))
        elif isFirst:
            prevLayer = input
            inpt = prevLayer
        for n in range(len(layer)):
            cneuron = layer[n]
            # Neuron for current layer is layer[n]
            # we want the CURRENT LAYER's WEIGHT to be
            # calculated with the previous layer's
            # output

            # Activation value = n1w1 * n2w2 + bias
            activationValue = 0
            activationValue += getBias() * \
                cneuron['weights'][len(cneuron['weights']) - 1]

            # pv = addVector(np.asfarray(cneuron['weights']), )            
            for pn in range(len(prevLayer)):
                # print(activationValue)
                activationValue += cneuron['weights'][pn] * \
                    inpt[pn]

            # USE CUDA
            # playerval = list(map(lambda x: x['value'], prevLayer))
            # cv = addVector(np.asfarray(cneuron['weights'][0:len(cneuron['weights']) - 1 ]),np.asfarray(playerval))
            # for v in cv: activationValue += v

            # check for activation function
            if cneuron['activation'] == 'sigmoid':
                cneuron['value'] = functions.sigmoid(activationValue)
            elif cneuron['activation'] == 'tanh':
                cneuron['value'] = functions.tanh(activationValue)
            elif cneuron['activation'] == 'relu':
                cneuron['value'] = functions.relu(activationValue)
            elif cneuron['activation'] == 'softmax':
                useSoftmax = True
                cneuron['activationValue'] = activationValue
            else:
                # default is sigmoid
                cneuron['value'] = functions.sigmoid(activationValue)

        # elif isFirst:
        #     prevLayer = input
        #     for n in range(len(layer)):
        #         cneuron = layer[n]
        #         # Neuron for current layer is layer[n]
        #         # we want the CURRENT LAYER's WEIGHT to be
        #         # calculated with the input layer's
        #         # output

        #         # Activation value = n1w1 * n2w2 + bias
        #         activationValue = 0
        #         activationValue += bias * cneuron['weights'][len(cneuron['weights']) - 1]

        #         for pn in range(len(prevLayer)):
        #             activationValue += cneuron['weights'][pn] * prevLayer[pn]
        #         # print(np.asfarray(prevLayer))
        #         # print(np.asfarray(cneuron['weights'][0:len(cneuron['weights']) - 1]))
        #         # USE CUDA
        #         # v = addVector(np.asfarray(cneuron['weights'][0:len(cneuron['weights']) - 1]),np.asfarray(prevLayer))
        #         # for av in v : activationValue = activationValue + av

        #         # check for activation function
        #         if cneuron['activation'] == 'sigmoid':
        #             cneuron['value'] = functions.sigmoid(activationValue)
        #         elif cneuron['activation'] == 'relu':
        #             cneuron['value'] = functions.relu(activationValue)
        #         elif cneuron['activation'] == 'tanh':
        #             cneuron['value'] = functions.tanh(activationValue)
        #         elif cneuron['activation'] == 'softmax':
        #             useSoftmax = True
        #             cneuron['activationValue'] = activationValue
        #         else:
        #             # default is sigmoid
        #             cneuron['value'] = functions.sigmoid(activationValue)
        if useSoftmax == True:
            accumulated = 0

            x = np.asfarray(list(map(lambda x: x['activationValue'], layer)))
            xo = np.exp(x - np.max(x))
            xout = xo / xo.sum()

            for n in range(len(layer)):
                layer[n]['value'] = xout[n]

            # print(list(map(lambda x : x['value'], layer)))

    return list(map(lambda x: x['value'], network[-1]))


def backward(network, expected):
    for i in reversed(range(len(network))):
        error = []
        clayer = network[i]
        if i == len(network) - 1:
            # output layer
            # compare this layer with expected output
            for n in range(len(clayer)):
                cneuron = clayer[n]
                err = expected[n] - cneuron['value']
                # err = ((expected[n] - cneuron['value'])**2)/2
                # err = -1 * expected[n] * math.log(cneuron['value'])
                # err = functions.CrossEntropy(cneuron['value'],expected[n])
                error.append(err)
        else:
            # hidden layer
            # compare this layer's output with prev layer
            for n in range(len(clayer)):
                er = 0.0
                nextLayer = network[i + 1]
                for nn in nextLayer:
                    er += nn['weights'][n] * nn['delta']

                error.append(er)

        # Not using numpy
        for n in range(len(clayer)):
            neuron = clayer[n]
            if neuron['activation'] == "sigmoid":
                neuron['delta'] = error[n] * \
                    functions.sigmoidD(neuron['value'])
            elif neuron['activation'] == "relu":
                neuron['delta'] = error[n] * functions.reluD(neuron['value'])
            elif neuron['activation'] == "tanh":
                neuron['delta'] = error[n] * functions.tanhD(neuron['value'])
            elif neuron['activation'] == "softmax":
                neuron['delta'] = error[n] * \
                    functions.reluD(neuron['activationValue'])


def updateWeights(network, input):
    for i in range(len(network)):
        # print("Layer %d " % i)
        if i != 0:
            input = list(map(lambda x: x['value'], network[i-1]))
        # print("Input : %s" % input)

        for neuron in network[i]:
            # print(neuron)
            for v in range(len(input)):
                # if(neuron['delta'] <= 0):
                #     continue
                neuron['weights'][v] += getLearningRate() * \
                    neuron['delta'] * input[v]

            # neuron['weights'][len(neuron['weights']) - 1] += learningRate * neuron['delta'] * bias
