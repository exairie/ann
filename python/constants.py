bias = 1
learningRate = 0.0001

def getLearningRate():
    global learningRate
    return learningRate

def setLearningRate(rate):    
    global learningRate
    learningRate = rate

def setBias(newbias):
    global bias
    bias = newbias

def getBias():
    global bias
    return bias