from functions import createLayer
from sys import argv
import numpy
from functions import peminatan,peminatanCreateOutput, peminatanParseOutput, getMaxArrayIndex
import json
from os import path
import constants
import propagation
from math import pow

# po = numpy.ones(30000, numpy.float32)
# print(po)
epoch = 10000
networkID = 'peminatan_regression_rpl'

if path.isfile(networkID):
    with open(networkID,'r') as net:
        network = json.loads(net.read())

else:
    network = []
    network.append(createLayer(9, 33, "sigmoid"))
    network.append(createLayer(15, 9, "sigmoid"))
    network.append(createLayer(1, 15, "sigmoid"))

def saveNetwork():
    with open(networkID,'w') as file:
        file.write(json.dumps(network))

data = peminatan()

def learn():
    llerror = 999999999999
    lerror = 99999999999
    ep = 0
    for ep in range(epoch):
        print("Epoch %d lrate : %.4f" % (ep,constants.learningRate), end="")
        errsum = 0.0
        for i in range(len(data)):
            arr = data[i][0:33]
            output = propagation.forward(network, arr)

            out = [1 if data[i][33].lower() == 'rpl' else 0]        
            for o in range(len(output)):
                errsum += pow(out[o] - output[o], 2) / 2
            
            propagation.backward(network, out)
            propagation.updateWeights(network, arr)

        saveNetwork()
        print(" sum_error : %.16f" % (errsum))
        ep += 1
        llerror = lerror
        lerror = errsum    

def test():
    rightCount = 0
    allCount = 0
    for i in range(len(data)):
        arr = data[i][0:33]
        output = propagation.forward(network,arr)
        result = getMaxArrayIndex(output)
        exp = data[i][33]
        out = output[0]
        print("expected %s -> %s result %.5f" % (exp,[1 if data[i][33].lower() == 'rpl' else 0],out))
        
        if exp == out:
            rightCount += 1
        allCount+=1

    print("All : %d Right : %d, accuracy : %.4f" %(allCount,rightCount, rightCount/allCount))


if len(argv) == 1:
    learn()
    test()
    
elif argv[1] == 'test':
    test()
    
else:
    learn()
    test()


