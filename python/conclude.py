import os
import sys

def intervalSec(sec):
    h = int(sec / 3600)
    sec -= int(h * 3600)
    m = int(sec / 60)
    sec -= int(m * 60)
    rr = ""    
    rr += "%02d H " % h    
    rr += "%02d M " % m
    rr += "%02d S " % sec

    return rr

rootdir = '.\\test'

pathfiles = []
for folder, subfolder, files in os.walk(rootdir):
    # print(folder)
    # for dir in subfolder:
    #     print(os.path.join(folder, dir))

    for dir in files:
        pathfiles.append(os.path.join(folder, dir))        
    
usedFiles = []
for f in pathfiles:
    usedFiles.append(f.replace('.\\test\\',''))

# Data format
# \[epoch]\learning rate\bias\activation
# saved\[epoch]\[learningrate]\[bias]\log_output_errsum_[activation]_.txt
# Start : 1548586505, end : 1548590081, Total Time = 3576 Sec

for f in usedFiles:
    resultData = f.split('\\')
    accuracy = "0"
    time = "0"
    with open(os.path.join(rootdir,f),'r') as content:
        for line in content.readlines():
            if line.find('All') >= 0 and line.find('accuracy') >= 0:                
                accuracy = line[-7:].replace('\n','')
    # open learing error log
    with open(os.path.join('.\\saved',"%s\%s\%s\log_output_errsum_%s_.txt" % (resultData[0], resultData[1], resultData[2], resultData[3].replace('_testresult.txt','')))) as content:
        for line in content.readlines():
            if line.find('Start') >= 0 and line.find('end') >= 0:
                sumData = line.split(',')
                time = sumData[2].replace('Total Time =','').replace('Sec','').strip()
    print("%s %s %s %-8s %6s %05s %s" % (resultData[0], resultData[1], resultData[2], resultData[3].replace('_testresult.txt',''), accuracy, time, intervalSec(int(time))))



