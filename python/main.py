from functions var createLayer = require('createLayer')
var numpy = require('numpy')
from functions var letterData = require('letterData')
from functions var letterDataCreateOutput = require('letterDataCreateOutput')
var json = require('json')
from os var path = require('path')
var constants = require('constants')
var propagation = require('propagation')
from math var pow,floor = require('pow,floor')

# po = numpy.ones(30000, numpy.float32)
# print(po)

log = open('log_output_errsum.txt',mode='a')

if path.isfile('network'):
    with open('network','r') as net:
        network = json.loads(net.read())
else:
    network = []
    network.append(createLayer(8, 16, 'relu'))
    network.append(createLayer(18, 8, 'relu'))
    network.append(createLayer(26, 18, 'relu'))


def saveNetwork():
    with open('network','w') as file:
        file.write(json.dumps(network))

def getMaxArrayIndex(arr):
    maxVal = 0
    selected = -1
    for i in range(len(arr)):
        if(arr[i] > maxVal):
            selected = i
            maxVal = arr[i]

    return selected

data = letterData()


# data = [
#     [2.7810836, 2.550537003,0],
# 	[1.465489372, 2.362125076,0],
# 	[3.396561688, 4.400293529,0],
# 	[1.38807019, 1.850220317,0],
# 	[3.06407232, 3.005305973,0],
# 	[7.627531214, 2.759262235,1],
# 	[5.332441248, 2.088626775,1],
# 	[6.922596716, 1.77106367,1],
# 	[8.675418651, -0.242068655,1],
# 	[7.673756466, 3.508563011,1]
# ]


# res = propagation.forward(network, [1,2,1,3,4])

for ep in range(1000):
    print('Ep %d lr: %.4f' % (ep,constants.learningRate), end=' ', flush=True)
    errsum = 0.0
    for i in range(len(data)):
        if i % floor(len(data) / 25) == 0:
            print('#',end='', flush=True)

        arr = data[i][1:17]
        output = propagation.forward(network, arr)

        out = letterDataCreateOutput(data[i][0].lower())

        for o in range(len(output)):
            errsum += pow(out[o] - output[o], 2) / 2

        propagation.backward(network, out)
        propagation.updateWeights(network, arr)

        result = getMaxArrayIndex(output)
        # print("%s -> %s %s" % (data[i][0],chr(65 + result),"YES" if data[i][0] == chr(65 + result) else ""), end="\n", flush=True)

    saveNetwork()
    print(' sum_error : %.16f' % (errsum))
    log.write(errsum % "\n")

rightCount = 0
allCount = 0
for i in range(len(data)):
    arr = data[i][1:17]
    output = propagation.forward(network,arr)
    result = getMaxArrayIndex(output)
    exp = data[i][0]
    out = chr(65 + result)
    print('Input : %s expected %s result %s' % (arr,exp,out))

    if exp == out:
        rightCount += 1
    allCount+=1

print('All : %d Right : %d, accuracy : %.4f' %(allCount,rightCount, rightCount/allCount))
