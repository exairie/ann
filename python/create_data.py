from random import random
from sys import argv
scoreRange = [2.3, 2.7, 3, 3.3, 3.7, 4]
classRange = ["RPL", "MM", "TKJ"]


def start(filename):
    numSample = 250
    isTest = False
    if len(argv) == 3:
        if str.isdigit(argv[2]):
            numSample = int(argv[2])
    print("Create data '%s' with %d sample data?" % (filename, numSample))
    i = input()
    with open('%s.data' % filename, 'a') as file:
        for i in range(0, numSample):
            scoreprob = [0.0,0.0,0.0] # RPL, MM, TKJ
            data = []
            for j in range(0, 34):
                score = float(scoreRangeRand())                
                data.append(score)
            print(data)
            # Apply each class bias probability
            # Index 6, 13, 19 representing RPL
            scoreprob[0] += data[6]  + data[13] + data[19]
            # Index 24, 28, 33 representing MM
            scoreprob[1] += data[24] + data[28] + data[33]
            # Index 7, 14, 23 representing TKJ
            scoreprob[2] += data[7]  + data[14] + data[23]
            # Randscore based on probability
            rclass = classRange[scoreprob.index(max(scoreprob))]
            data = list(map(lambda x : str(x), data))
            data.append(rclass)

            file.writelines("%s\n" % (','.join(data)))


def scoreRangeRand():
    i = int(random() * (len(scoreRange)))
    return str(scoreRange[i])


def classRangeRand():
    i = int(random() * (len(classRange)))
    return classRange[i]


filename = argv[1] if len(argv) >= 1 else 'generated'
start(filename)
